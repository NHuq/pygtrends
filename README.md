Web scraping tool for pulling data for:

 - Single search term
 - Broken down by day and hour
 - Between date ranges of your choice
 - Normalise the data form multiple date windows
 - Name your data file name, and normalised data file name
 
To run a search, doubleclick the '1) Run query.bat' file. A terminal window will open up where you will be asked for:

 - keyword
 - start date
 - end date
 - file name for saved data
 
 The script will then run. It will run a ghost browser, load each query URL, then pull the data within the ghost page. Data will be saved in the '\data' folder.\n
 The script take every day in the date range, runs a search over a 7 day period, then shifts to date range to the next day.
 
 The terminal outputs the date window and url of the search period's start/end date, as well as the url. Depending on the size of the job, this will take time.
 
 Indexed data between all the date queries can then be normalised by doubleclicking 'Normalise.bat' file. A terminal window will open up asking:
 
 - Enter a central date for the data to normalise around
 - The filename you'd like normalised data to be saved under
 
 The script will then run - putting together all data files in the '/data' folder and working out normalised values for each date-hour timestamp. The file /n
 will then save in the main directory for your enjoyment.
 
 Notes:
 It's unclear how Google will manage multiple simillar search queries handled by this tool. If you are pulling data over a long date range, it's best to break the
 job up e.g. by month.
 
 If you are breaking the query down into mulitple files, make sure they are named in such a way that the 'Normalise' script can order the files correctly e.g. name
 the first file with '1_', then '2_' and so on.
 