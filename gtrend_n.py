import pandas as pd
import os

c_date = input('Enter date to normalise to yyyy-mm-dd:')
file_list = os.listdir('data')
fileName = input('Enter file name to save: ')
#read data csvs

data = pd.DataFrame()

for i in file_list:
    d = pd.read_csv('data\\' + i)
    d = d[d['timestamp']!='0']
    d = d[list(d)[1:]]
    if file_list.index(i) == 0:
        data = d
    else:
        data = data.merge(d, how='outer', on='timestamp')

#date handling

s = pd.to_datetime(c_date)
date_range = pd.date_range(s-pd.Timedelta('1 days'),periods=3)

#Normalise left/right flanking columns

col_num =  list(data).index(c_date)

data_t = data

for i in range(1,col_num):
    da = data_t[list(data_t)[col_num-i+1]]
    db = data_t[list(data_t)[col_num-i]]
    t = da[da>0]/db[da[da>0].index.values]
    t = t[t!=float('inf')].mean()
    dn = db*t
    data_t[list(data_t)[col_num-i]] = dn

for i in range(col_num+1,len(list(data_t))):
    da = data_t[list(data_t)[i-1]]
    db = data_t[list(data_t)[i]]
    t = da[da>0]/db[da[da>0].index.values]
    t = t[t!=float('inf')].mean()
    dn = db*t
    data_t[list(data_t)[i]] = dn

#average normalised values

search_ts = pd.DataFrame(data_t['timestamp'])
search_ts['search'] = data_t[list(data_t)[1:]].mean(axis=1)
search_ts.to_csv(fileName + '.csv')

print('complete')