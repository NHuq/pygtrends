import urllib.request
from selenium import webdriver 
import pandas as pd
import datetime as dt
import re
from time import sleep
import os

#inputs

kw = input('Enter keyword: ')
start = input('enter start date yyyy-mm-dd: ')
end = input('enter end date yyyy-mm-dd: ')

fileName = input('Enter file name for data: ') + '.csv'

#date handling

start_date = pd.to_datetime(start)
end_date = pd.to_datetime(end)
td = end_date-start_date

date_range = pd.date_range(start_date,periods=td.days+1)

#web scraper setup
log = []
paths = {}
kw_e = kw.replace(' ','%20')

o = webdriver.ChromeOptions()
o.add_argument('headless')
o.add_argument('log-level=3')
w = webdriver.Chrome(chrome_options=o)

print('Starting Job')
#web scraping loop

for i in date_range:

    a = str(i)[:10]
    b = str(i+pd.Timedelta('7 days'))[:10]
    url = 'https://trends.google.com/trends/explore?date='+a+'T0%20'+b+'T23&geo=GB&q='+kw_e+''
    w.get(url)
    sleep(2)
    log.append(a+b+url)
    print(a,b,url)
    x=0
    while x==0:
        try:
            w.execute_script("return document.getElementsByClassName('line-chart').item(0).getElementsByTagName('path').item(0).getAttribute('d')")
            x=1
        except:
            w.get(url)
            sleep(2)
            x = 0
    p = w.execute_script("return document.getElementsByClassName('line-chart').item(0).getElementsByTagName('path').item(0).getAttribute('d')")
    paths[a] = p

w.close()

#convert path attributes to pixel values
values={}

for key in paths:
    v = paths[key].split('L')
    values[key] = v

#convert pixel values to just y values
data = {}
for key, value in values.items():
    entry = [0]*((len(date_range)*24+168))
    pos1 = list(date_range).index(pd.Timestamp(key))*24
    frag = []
    for i in value:
        frag.append(float(i[i.find(',') + 1:]))
    entry[pos1:pos1+len(value)] = frag
    data[key] = entry

#transform y pixel values to index between 0-1
log = pd.DataFrame(log)
data = pd.DataFrame(data)

zero = data.replace(0,100000000).min().min()
hundred = data.max().max()

data_0 = data[data!=0]
data_t = 1-((data_0-zero)/(hundred-zero))
data_t.fillna(0)

#add column for date and hour timestamp

hours = []
for i in range(0,24):
    hours.append(i)
    
time_r = []
tail_dates = pd.date_range(date_range[len(date_range)-1]+pd.Timedelta('1 days'),periods=7)
date_range = date_range.append(tail_dates)

for x in date_range:
    for y in hours:
        a = x + pd.Timedelta(str(y)+'hours')
        time_r.append(a)

time_r = time_r+([0]*(len(data_t)-len(time_r)))

data_t['timestamp'] = time_r
data_t = data_t[['timestamp']+list(data_t)[:len(list(data_t))-1]]

#file handling

try: 
    os.listdir().index('data')
except:
    os.mkdir('data')

try:
    os.listdir().index('logs')
except:
    os.mkdir('logs')

data_t.to_csv('data\\' + fileName)
log.to_csv('logs\\'+ fileName + '_log.csv')

print('Complete')
exit()